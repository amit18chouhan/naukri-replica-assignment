package com.naukri.replica.naukrireplicaassignment.mappers;

import com.naukri.replica.naukrireplicaassignment.domain.User;
import com.naukri.replica.naukrireplicaassignment.domain.UserKey;
import com.naukri.replica.naukrireplicaassignment.model.UserDTO;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static User maptToUserDomain(UserDTO userDto) {
        User user = new User();
        user.setUserKey(new UserKey(userDto.getEmailId()));
        user.setSalutation(userDto.getSalutation());
        user.setFirstName(userDto.getFirstName());
        user.setMiddleName(userDto.getMiddleName());
        user.setLastName(userDto.getLastName());
        user.setGender(userDto.getGender());
        user.setEmailId(userDto.getEmailId());
        user.setMobileNumber(userDto.getMobileNumber());
        user.setLocation(userDto.getLocation());
        user.setYearsOfExperience(userDto.getYearsOfExperience());
        user.setWorkExperiences(userDto.getWorkExperiences());
        user.setSkills(userDto.getSkills());
        return user;
    }

    public static UserDTO maptToUserDto(User user) {
        UserDTO userDto = new UserDTO();
        userDto.setSalutation(user.getSalutation());
        userDto.setFirstName(user.getFirstName());
        userDto.setMiddleName(user.getMiddleName());
        userDto.setLastName(user.getLastName());
        userDto.setGender(user.getGender());
        userDto.setEmailId(user.getEmailId());
        userDto.setMobileNumber(user.getMobileNumber());
        userDto.setLocation(user.getLocation());
        userDto.setYearsOfExperience(user.getYearsOfExperience());
        userDto.setWorkExperiences(user.getWorkExperiences());
        userDto.setSkills(user.getSkills());
        return userDto;
    }

}
