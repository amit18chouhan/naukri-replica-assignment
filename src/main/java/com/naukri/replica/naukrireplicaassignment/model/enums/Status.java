package com.naukri.replica.naukrireplicaassignment.model.enums;

public enum Status {
    /*
     * P= Profile Pending
     * A= Profile Active
     * D = Profile Deleted
     */

    P, A, D;
}
