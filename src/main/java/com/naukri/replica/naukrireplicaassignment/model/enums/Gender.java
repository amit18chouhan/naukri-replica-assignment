package com.naukri.replica.naukrireplicaassignment.model.enums;

public enum Gender {
    MALE, FEMALE, OTHERS;
}
