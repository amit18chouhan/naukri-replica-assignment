package com.naukri.replica.naukrireplicaassignment.model.enums;

public enum EmploymentType {
    PERMANENT, CONTRACT;
}
