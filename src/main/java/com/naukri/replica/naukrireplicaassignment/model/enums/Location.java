package com.naukri.replica.naukrireplicaassignment.model.enums;

public enum Location {
    /**
     * Location is to check that current profile person is located in India or Outside
     */
    India, Outside;
}
