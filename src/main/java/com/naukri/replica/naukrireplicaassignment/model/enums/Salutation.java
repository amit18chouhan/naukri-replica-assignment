package com.naukri.replica.naukrireplicaassignment.model.enums;

public enum Salutation {

    Mr, Ms, Mrs, Dr;
}
