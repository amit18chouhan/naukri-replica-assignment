package com.naukri.replica.naukrireplicaassignment.repository;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.naukri.replica.naukrireplicaassignment.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.UpdateDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRespositoryV1 {
    private Logger LOGGER = LoggerFactory.getLogger(UserRespositoryV1.class);

    private MongoTemplate mongoTemplate;

    @Autowired
    public UserRespositoryV1(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<User> getAllUsers() {
        Query query = new Query();
        query.addCriteria(Criteria.where("status").in("A", "P"));
        List<User> users = mongoTemplate.find(query, User.class);
        return users;
    }

    public User getUserByEmail(User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userKey.userKey").is(user.getEmailId()));
        query.addCriteria(Criteria.where("status").in("A", "P"));

        List<User> users = mongoTemplate.find(query, User.class);
        return users.size() > 0 ? users.get(0) : null;
    }

    public User insertUser(User user) {
        return mongoTemplate.save(user);
    }

    public User updateUser(User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userKey.userKey").is(user.getEmailId()));
        query.addCriteria(Criteria.where("status").in("A"));

        Update update = new Update().set("status", "D");

        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, User.class);
        return updateResult.getModifiedCount() > 0 ? user : null;
    }

    public boolean deleteUser(User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userKey.userKey").is(user.getEmailId()));
        query.addCriteria(Criteria.where("status").in("D"));

        DeleteResult deleteResult = mongoTemplate.remove(query, User.class);
        return deleteResult.getDeletedCount() > 0 ? true : false;
    }
}
