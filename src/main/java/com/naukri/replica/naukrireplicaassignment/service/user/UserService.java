package com.naukri.replica.naukrireplicaassignment.service.user;

import com.naukri.replica.naukrireplicaassignment.domain.User;
import com.naukri.replica.naukrireplicaassignment.exceptions.RecordsException;
import com.naukri.replica.naukrireplicaassignment.exceptions.UserAlreadyExists;
import com.naukri.replica.naukrireplicaassignment.exceptions.UserNotFoundException;
import com.naukri.replica.naukrireplicaassignment.mappers.UserMapper;
import com.naukri.replica.naukrireplicaassignment.model.UserDTO;
import com.naukri.replica.naukrireplicaassignment.model.enums.Status;
import com.naukri.replica.naukrireplicaassignment.repository.UserRespositoryV1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

import static com.naukri.replica.naukrireplicaassignment.constants.UserConstants.USER_ALREADY_EXISTS;

@Service
public class UserService implements IUserService {

    private UserRespositoryV1 userRespositoryV1;
    private UserMapper userMapper;
    private Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(UserRespositoryV1 userRespositoryV1, UserMapper userMapper) {
        this.userRespositoryV1 = userRespositoryV1;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDTO> getUsers() {
        List<User> users = userRespositoryV1.getAllUsers();
        List<UserDTO> userDTOS = users.stream().map(user -> userMapper.maptToUserDto(user)).collect(Collectors.toList());
        return userDTOS;
    }

    @Override
    public UserDTO addUser(UserDTO userDTO) throws UserAlreadyExists {
        try {

            User user = userMapper.maptToUserDomain(userDTO);
            user.setStatus(Status.A);
            IValidationService validationService = validateUser -> this.userRespositoryV1.getUserByEmail(validateUser) != null ? true : false;
            if (!validationService.validateDuplicateProfile(user)) {
                User addedUser = userRespositoryV1.insertUser(user);
                return userMapper.maptToUserDto(addedUser);
            } else {
                LOGGER.info(USER_ALREADY_EXISTS + "{}", user.getEmailId());
                throw new UserAlreadyExists(USER_ALREADY_EXISTS + user.getEmailId());
            }

        } catch (Exception e) {
            LOGGER.error("Error Inserting Record to DB for User : {}", userDTO);
        }
        return null;
    }

    @Override
    public UserDTO getUserByEmail(UserDTO userDto) {
        User user = userMapper.maptToUserDomain(userDto);
        User dbUserRecord = userRespositoryV1.getUserByEmail(user);
        return userMapper.maptToUserDto(dbUserRecord);
    }

    @Override
    public UserDTO updateUserInfo(UserDTO userDto) throws RecordsException, UserNotFoundException, UserAlreadyExists {
        User user = userMapper.maptToUserDomain(userDto);
        if (StringUtils.hasLength(user.getEmailId())) {
            User dbUser = this.userRespositoryV1.getUserByEmail(user);
            if (dbUser != null) {
                dbUser = mapUserForUpdate(dbUser, user);
                this.userRespositoryV1.updateUser(dbUser);
                addUser(userDto);
                
            } else {
                LOGGER.info("User with EmailId: {} not found in DB", user.getEmailId());
                throw new UserNotFoundException("User with emailId not present in DB.");
            }
        } else {
            LOGGER.info("Empty Email found for User: {}", userDto.toString());
            throw new RecordsException("Cannot pass empty/null emailId");
        }
        return userDto;
    }


    private User mapUserForUpdate(User dbUser, User currentUser) throws RecordsException {
        if (dbUser != null) {
            if (StringUtils.hasLength(currentUser.getSalutation().toString())) {
                dbUser.setSalutation(currentUser.getSalutation());
            }
            if (StringUtils.hasLength(currentUser.getFirstName())) {
                dbUser.setFirstName(currentUser.getFirstName());
            }
            if (StringUtils.hasLength(currentUser.getMiddleName())) {
                dbUser.setMiddleName(currentUser.getMiddleName());
            }
            if (StringUtils.hasLength(currentUser.getLastName())) {
                dbUser.setLastName(currentUser.getLastName());
            }
            if (StringUtils.hasLength(currentUser.getGender().toString())) {
                dbUser.setGender(currentUser.getGender());
            }
            /*if(StringUtils.hasLength(currentUser.getEmailId())){
                dbUser.setEmailId(currentUser.getEmailId());
            }*/
            if (StringUtils.hasLength(currentUser.getMobileNumber())) {
                dbUser.setMobileNumber(currentUser.getMobileNumber());
            }
            if (StringUtils.hasLength(currentUser.getLocation().toString())) {
                dbUser.setLocation(currentUser.getLocation());
            }
            if (currentUser.getSkills() != null && currentUser.getSkills().size() > 0) {
                dbUser.setSkills(currentUser.getSkills());
            }

            return dbUser;
        } else {
            throw new RecordsException("No Record found for update");
        }
    }
}
