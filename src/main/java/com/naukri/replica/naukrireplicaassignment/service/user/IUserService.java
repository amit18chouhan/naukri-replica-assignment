package com.naukri.replica.naukrireplicaassignment.service.user;

import com.naukri.replica.naukrireplicaassignment.exceptions.RecordsException;
import com.naukri.replica.naukrireplicaassignment.exceptions.UserAlreadyExists;
import com.naukri.replica.naukrireplicaassignment.exceptions.UserNotFoundException;
import com.naukri.replica.naukrireplicaassignment.model.UserDTO;

import java.util.List;

public interface IUserService {

    /***
     * To get all users with statuc 'A', 'p' and 'D' status
     * @return List
     */
    public List<UserDTO> getUsers();

    /***
     * Create User
     * @return
     */
    public UserDTO addUser(UserDTO user) throws UserAlreadyExists;

    /***
     * get user details by email
     * @param user
     * @return
     */
    public UserDTO getUserByEmail(UserDTO user);

    /***
     * update existing userInfo
     * @param user
     * @return
     */
    public UserDTO updateUserInfo(UserDTO user) throws RecordsException, UserNotFoundException, UserAlreadyExists;

}
