package com.naukri.replica.naukrireplicaassignment.service.user;

import com.naukri.replica.naukrireplicaassignment.domain.User;

public interface IValidationService {

    /**
     * Validation class to validate new user creation on basis of email
     *
     * @param user
     * @return
     */
    public boolean validateDuplicateProfile(User user);
}
