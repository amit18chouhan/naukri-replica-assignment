package com.naukri.replica.naukrireplicaassignment.exceptions;

public class UserAlreadyExists extends Exception {
    public UserAlreadyExists(String errorMessage) {
        super(errorMessage);
    }
}
