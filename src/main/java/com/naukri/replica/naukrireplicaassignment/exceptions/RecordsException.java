package com.naukri.replica.naukrireplicaassignment.exceptions;

public class RecordsException extends Exception {

    public RecordsException(String errorMessage) {
        super(errorMessage);
    }

}
