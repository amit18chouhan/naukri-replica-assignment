package com.naukri.replica.naukrireplicaassignment.domain;

import com.naukri.replica.naukrireplicaassignment.model.enums.Gender;
import com.naukri.replica.naukrireplicaassignment.model.enums.Location;
import com.naukri.replica.naukrireplicaassignment.model.enums.Salutation;
import com.naukri.replica.naukrireplicaassignment.model.enums.Status;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Document(collection = "USER")
public class User {
    private UserKey userKey;
    private Salutation salutation;
    private String firstName;
    private String middleName;
    private String lastName;
    private Gender gender;
    private String emailId;
    private String mobileNumber;
    private Location location;
    private Status status;
    private Float yearsOfExperience;
    private List<UserWorkExperience> workExperiences;
    private List<String> skills;

    public UserKey getUserKey() {
        return userKey;
    }

    public void setUserKey(UserKey userKey) {
        this.userKey = userKey;
    }

    public Salutation getSalutation() {
        return salutation;
    }

    public void setSalutation(Salutation salutation) {
        this.salutation = salutation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Float getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(Float yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public List<UserWorkExperience> getWorkExperiences() {
        return workExperiences;
    }

    public void setWorkExperiences(List<UserWorkExperience> workExperiences) {
        this.workExperiences = workExperiences;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User that = (User) o;
        return Objects.equals(this.userKey, that.userKey) && Objects.equals(this.mobileNumber, that.mobileNumber);
    }

    @Override
    public String toString() {
        return "User{" +
                "userKey=" + userKey +
                ", salutation=" + salutation +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", emailId='" + emailId + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", location=" + location +
                ", status=" + status +
                ", yearsOfExperience" + yearsOfExperience +
                ", workExperiences" + workExperiences +
                ", skills=" + skills +
                '}';
    }
}
