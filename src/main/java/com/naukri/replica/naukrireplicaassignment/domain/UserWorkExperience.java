package com.naukri.replica.naukrireplicaassignment.domain;

import com.naukri.replica.naukrireplicaassignment.model.enums.EmploymentType;

import java.time.LocalDate;
import java.util.Objects;

public class UserWorkExperience {
    private String nameOfOrg;
    private LocalDate startDate;
    private LocalDate endDate;
    private EmploymentType employmentType;

    public String getNameOfOrg() {
        return nameOfOrg;
    }

    public void setNameOfOrg(String nameOfOrg) {
        this.nameOfOrg = nameOfOrg;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserWorkExperience that = (UserWorkExperience) o;
        return nameOfOrg.equals(that.nameOfOrg) && startDate.equals(that.startDate) && endDate.equals(that.endDate) && employmentType == that.employmentType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfOrg, startDate, endDate, employmentType);
    }

    @Override
    public String toString() {
        return "UserWorkExperience{" +
                "nameOfOrg='" + nameOfOrg + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", employmentType=" + employmentType +
                '}';
    }
}
