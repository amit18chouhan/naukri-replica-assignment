package com.naukri.replica.naukrireplicaassignment.domain;

import java.util.Objects;

public class UserKey {
    private String userKey;

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public UserKey() {
        /*default constructor*/
    }

    public UserKey(String userKey) {
        this.userKey = userKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        return Objects.equals(userKey, ((UserKey) o).userKey);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userKey);
    }

    @Override
    public String toString() {
        return "UserKey{" +
                "userKey='" + userKey + '\'' +
                '}';
    }

}
