package com.naukri.replica.naukrireplicaassignment.controller;

import com.naukri.replica.naukrireplicaassignment.exceptions.UserAlreadyExists;
import com.naukri.replica.naukrireplicaassignment.model.UserDTO;
import com.naukri.replica.naukrireplicaassignment.service.user.IUserService;
import com.naukri.replica.naukrireplicaassignment.service.user.UserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/user")
public class UserController {

    private Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private IUserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "getUsers", nickname = "Get all Users which are present in DB.")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUsers() {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUsers());
    }

    @ApiOperation(value = "createUser", nickname = "Create User.")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@RequestBody UserDTO user) {
        try {
            LOGGER.info("Inside Create User");
            UserDTO returnValue = this.userService.addUser(user);
            if (returnValue != null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(user);
            } else {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Error Creating User");
            }
        } catch (UserAlreadyExists e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @ApiOperation(value = "updateUser", nickname = "Update user.")
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@RequestBody UserDTO user) {

        try {
            UserDTO updatedUser = this.userService.updateUserInfo(user);
            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating user record");
        }

    }

    @ApiOperation(value = "getUsers", nickname = "Get all Users which are present in DB.")
    @RequestMapping(value = "/email/{emailId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUsersByEmailId(@PathVariable String emailId) {
        UserDTO user = new UserDTO();
        user.setEmailId(emailId);
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserByEmail(user));
    }
}
